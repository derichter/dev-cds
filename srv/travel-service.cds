using { db as my } from '../db/schema';

service TravelService {
  entity Travel as projection on my.Travel actions {
    action getBookings() returns Travel;
  };

  entity Passenger as projection on my.Passenger;
  entity BookingStatus as projection on my.BookingStatus;

  entity Booking as projection on my.Booking;

  entity BookingSelect as select from Booking { BookingID };

  entity BookingWhere as select from Booking { * } where ConnectionID='0002';

  entity BookingJoin as select Booking.BookingStatus, BookingStatus.name as BookingStatus_text
                          from Booking left join BookingStatus
                            on BookingStatus.code = Booking.BookingStatus.code;

  entity BookingExists as select *, Booking.to_Customer.LastName from Booking where exists (
    select * from Passenger where Passenger.CustomerID = Booking.to_Customer.CustomerID
                              and Passenger.LastName = 'Buchholm'
  );

  annotate my.MasterData with @cds.autexopose @readonly;
}
