using { Country, Currency, managed, sap.common.CodeList } from '@sap/cds/common';

namespace db;

aspect MasterData {}

entity Travel : managed {
  key TravelUUID : UUID;
  TravelID       : Integer;
  BeginDate      : Date;
  EndDate        : Date;
  BookingFee     : Decimal(16, 3);
  TotalPrice     : Decimal(16, 3);
  CurrencyCode   : Currency;
  Description    : String(1024);
  TravelStatus   : Association to TravelStatus @readonly;
  to_Agency      : Association to TravelAgency;
  to_Customer    : Association to Passenger;
  to_Booking     : Composition of many Booking on to_Booking.to_Travel = $self;
};

entity Booking : managed {
  key BookingUUID   : UUID;
  BookingID         : Integer;
  BookingDate       : Date;
  ConnectionID      : String(4);
  FlightDate        : Date;
  FlightPrice       : Decimal(16, 3);
  CurrencyCode      : Currency;
  BookingStatus     : Association to BookingStatus;
  to_BookSupplement : Composition of many BookingSupplement on to_BookSupplement.to_Booking = $self;
  to_Carrier        : Association to Airline;
  to_Customer       : Association to Passenger;
  to_Travel         : Association to Travel;
  to_Flight         : Association to Flight on  to_Flight.AirlineID = to_Carrier.AirlineID
                                            and to_Flight.FlightDate = FlightDate
                                            and to_Flight.ConnectionID = ConnectionID;
};

entity BookingSupplement : managed {
  key BookSupplUUID   : UUID;
  BookingSupplementID : Integer;
  Price               : Decimal(16, 3);
  CurrencyCode        : Currency;
  to_Booking          : Association to Booking;
  to_Travel           : Association to Travel;
  to_Supplement       : Association to Supplement;
};

entity Airline : MasterData {
  key AirlineID : String(3);
  Name          : String(40);
  CurrencyCode  : Currency;
  AirlinePicURL : String;
};

entity Airport : MasterData {
  key AirportID : String(3);
  Name          : String(40);
  City          : String(40);
  CountryCode   : Country;
};


entity Supplement : managed, MasterData {
  key SupplementID : String(10);
  Price            : Decimal(16, 3);
  Type             : Association to SupplementType;
  Description      : localized String(1024);
  CurrencyCode     : Currency;
};

entity Flight : MasterData {
  key AirlineID    : String(3);
  key FlightDate   : Date;
  key ConnectionID : String(4);
  Price            : Decimal(16, 3);
  CurrencyCode     : Currency;
  PlaneType        : String(10);
  MaximumSeats     : Integer;
  OccupiedSeats    : Integer;
  to_Airline       : Association to Airline on to_Airline.AirlineID = AirlineID;
  to_Connection    : Association to FlightConnection on  to_Connection.AirlineID = AirlineID
                                                     and to_Connection.ConnectionID = ConnectionID;
};

entity FlightConnection : MasterData {
  key ConnectionID   : String(4);
  key AirlineID      : String(3);
  DepartureAirport   : Association to Airport;
  DestinationAirport : Association to Airport;
  DepartureTime      : Time;
  ArrivalTime        : Time;
  Distance           : Integer;
  DistanceUnit       : String(3);
  to_Airline         : Association to Airline on to_Airline.AirlineID = AirlineID;
};

entity Passenger : managed, MasterData {
  key CustomerID : String(6);
  FirstName      : String(40);
  LastName       : String(40);
  Title          : String(10);
  Street         : String(60);
  PostalCode     : String(10);
  City           : String(40);
  CountryCode    : Country;
  PhoneNumber    : String(30);
  EMailAddress   : String(256);
};

entity TravelAgency : MasterData {
  key AgencyID : String(6);
  Name         : String(80);
  Street       : String(60);
  PostalCode   : String(10);
  City         : String(40);
  CountryCode  : Country;
  PhoneNumber  : String(30);
  EMailAddress : String(256);
  WebAddress   : String(256);
};

entity BookingStatus : CodeList {
  key code : String enum {
    New      = 'N';
    Booked   = 'B';
    Canceled = 'X';
  };
};

entity TravelStatus : CodeList {
  key code : String enum {
    Open     = 'O';
    Accepted = 'A';
    Canceled = 'X';
  } default 'O'; //> will be used for foreign keys as well
  criticality : Integer; //  2: yellow colour,  3: green colour, 0: unknown
  fieldControl: Integer; // 1: #ReadOnly, 7: #Mandatory
  createDeleteHidden: Boolean;
  insertDeleteRestriction: Boolean; // = NOT createDeleteHidden
};

entity SupplementType : CodeList {
  key code : String enum {
    Beverage = 'BV';
    Meal     = 'ML';
    Luggage  = 'LU';
    Extra    = 'EX';
  };
};
