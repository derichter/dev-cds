const cds = require('@sap/cds')

class TravelService extends cds.ApplicationService { 
  init() {
    const { Travel, Booking, BookingSupplement } = this.entities

    this.before ('NEW', 'Travel', async req => {
      req.data.Description = "New Travel";
    })
    
    this.on ('getBookings', async req => {
      const [{ TravelUUID, IsActiveEntity }] = req.params
      return this.read(Booking)
    })

    return super.init()
  }
}

module.exports = { TravelService }
